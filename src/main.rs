extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::WindowSettings;

use std::collections::linked_list::*;
use std::iter::FromIterator;
use rand::Rng;
use rand::prelude::ThreadRng;
use std::time::Instant;

const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
const BLUE: [f32; 4] = [0.0, 0.0, 1.0, 1.0];

#[derive(Clone, PartialEq)]
struct Point {
    x: u32,
    y: u32,
}

struct Food {
    scale: u32,
    grid: u32,
    position: Point,
}

impl Food {
    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs)
    {
        use graphics::*;
        let square = rectangle::square((self.scale * self.position.x) as f64, (self.scale * self.position.y) as f64, self.scale as f64);
        gl.draw(args.viewport(), |c, gl| {
            rectangle(BLUE, square, c.transform, gl);
        });
    }

    fn update(&self, point: &Point) -> bool {
        point == &self.position
    }

    fn generate_position(&mut self, points: &LinkedList<Point>) {
        loop {
            let mut rng: ThreadRng = rand::thread_rng();
            self.position.x = rng.gen_range(0, self.grid);
            self.position.y = rng.gen_range(0, self.grid);
            for slice in points {
                if &self.position == slice {
                    continue;
                }
            }
            break;
        }
    }
}

#[derive(Clone, PartialEq)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

struct Snake {
    scale: u32,
    grid: u32,
    body: LinkedList<Point>,
    mid_state_dir: Direction,
    dir: Direction,
}

impl Snake {
    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics::*;

        let squares: Vec<types::Rectangle> = self.body.iter().map(|point| {
            rectangle::square((self.scale * point.x) as f64, (self.scale * point.y) as f64, self.scale as f64)
        }).collect();

        gl.draw(args.viewport(), |c, gl| {
            let transform = c.transform;

            squares.into_iter().for_each(|square| rectangle(RED, square, transform, gl));
        });
    }

    fn update(&mut self, point_taken: bool) -> Result<(), &str> {
        let mut new_head: Point = self.get_head().clone();

        let last_dir = self.dir.clone();
        self.dir = self.mid_state_dir.clone();

        let touch_the_wall_error = Err("You've touched the wall!");

        match self.dir {
            Direction::Left if new_head.x == 0 => touch_the_wall_error,
            Direction::Right if new_head.x == self.grid - 1 => touch_the_wall_error,
            Direction::Up if new_head.y == 0 => touch_the_wall_error,
            Direction::Down if new_head.y == self.grid - 1 => touch_the_wall_error,
            _ => Ok(()),
        }?;

        match self.dir {
            Direction::Left if last_dir != Direction::Right => new_head.x -= 1,
            Direction::Right if last_dir != Direction::Left => new_head.x += 1,
            Direction::Up if last_dir != Direction::Down => new_head.y -= 1,
            Direction::Down if last_dir != Direction::Up => new_head.y += 1,
            _ => (),
        };

        if self.is_colliding(&new_head) {
            return Err("You've bitten yourself!");
        }

        self.body.push_front(new_head);

        if !point_taken {
            self.body.pop_back().unwrap();
        }

        Ok(())
    }

    fn get_body(&self) -> &LinkedList<Point> {
        &self.body
    }

    fn get_head(&self) -> &Point {
        self.get_body().front().expect("Snake has no body!")
    }

    fn is_colliding(&self, point: &Point) -> bool {
        for slice in &self.body {
            if point == slice {
                return true;
            }
        }
        return false;
    }
}

struct Engine {
    gl: GlGraphics,
    snake: Snake,
    food: Food,
    points: u32,
    point_taken: bool,
}

impl Engine {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        self.gl.draw(args.viewport(), |_c, gl| {
            clear(GREEN, gl);
        });

        self.snake.render(&mut self.gl, args);
        self.food.render(&mut self.gl, args);
    }

    fn update(&mut self, _args: &UpdateArgs) -> Result<(), &str> {
        self.snake.update(self.point_taken)?;

        if self.point_taken {
            self.point_taken = false;
        }

        Ok(())
    }

    fn update_food(&mut self, _args: &UpdateArgs) {
        if self.food.update(self.snake.get_head()) {
            self.food.generate_position(self.snake.get_body());
            self.points += 1;
            self.point_taken = true;
        }
    }

    fn pressed(&mut self, button: &Button) {
        let last_dir = self.snake.dir.clone();

        self.snake.mid_state_dir = match button {
            Button::Keyboard(Key::Left) if last_dir != Direction::Right => Direction::Left,
            Button::Keyboard(Key::Right)if last_dir != Direction::Left => Direction::Right,
            Button::Keyboard(Key::Up)if last_dir != Direction::Down => Direction::Up,
            Button::Keyboard(Key::Down) if last_dir != Direction::Up => Direction::Down,
            _ => last_dir,
        }
    }
}

fn main() {
    let opengl = OpenGL::V4_5;

    let scale = 30;
    let grid = 11;

    let mut window: GlutinWindow = WindowSettings::new("Snake", [scale * grid, scale * grid])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut engine = Engine {
        gl: GlGraphics::new(opengl),
        snake: Snake {
            scale,
            grid,
            body: LinkedList::from_iter((vec![Point { x: 0, y: grid / 2 }]).into_iter()),
            mid_state_dir: Direction::Right,
            dir: Direction::Right,
        },
        food: Food {
            scale,
            grid,
            position: Point { x: 0, y: 0 },
        },
        points: 0,
        point_taken: false,
    };

    engine.food.generate_position(engine.snake.get_body());

    let mut events = Events::new(EventSettings::new()).ups(5);
    let timer = Instant::now();
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            engine.render(&args);
        }
        if let Some(args) = e.update_args() {
            if let Err(err) = engine.update(&args) {
                println!("{}", err);
                break;
            }
            engine.update_food(&args);
        }
        if let Some(k) = e.button_args() {
            if k.state == ButtonState::Press {
                engine.pressed(&k.button);
            }
        }
    }
    println!("Game Over! Lasted for {} seconds with score: {}", timer.elapsed().as_secs(), engine.points);
}
